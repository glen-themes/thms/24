![Screenshot preview of the theme "Lachesis" by glenthemes](https://static.tumblr.com/gtjt4bo/0KIscje5a/lachesispv01.png)

**Theme no.:** 24  
**Theme name:** Lachesis  
**Theme type:** Free / Tumblr use  
**Description:** A header + sidebar theme with an artwork display, searchbar, custom title and description areas, and up to 10 links for your socials.  
**Author:** @&hairsp;glenthemes  

**Release date:** 2017-10-16  
**Rework date:** 2024-04-27  
**Last updated:** 2024-05-11

**Post:** [glenthemes.tumblr.com/post/166456102679](https://glenthemes.tumblr.com/post/166456102679)  
**Preview:** [glenthpvs.tumblr.com/lachesis](https://glenthpvs.tumblr.com/lachesis)  
**Download:** [glen-themes.gitlab.io/thms/24/lachesis](https://glen-themes.gitlab.io/thms/24/lachesis)
