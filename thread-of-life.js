/*------------------------------------

  DO NOT TAKE OR COPY.
  CREDITS & RESOURCES LISTED AT:
  glencredits.tumblr.com/lachesis
  
------------------------------------*/

let currentURL = window.location.href.replace(/[?#].*|\/$/,"");
let currentPath = window.location.pathname.replace(/\/$/,"");
document.documentElement.setAttribute("current-path",currentPath);

const loadImage = (url) => new Promise((resolve, reject) => {
  let img = new Image();
  img.addEventListener("load", () => resolve(img));
  img.addEventListener("error", err => reject(err));
  img.src = url;
});

const screenDims = () => {
  let vpw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
  let vph = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);
  setRoot("--100vw",`${vpw}px`);
  setRoot("--100vh",`${vph}px`);
  setRoot("--1vw",`${vpw*0.01}px`);
  setRoot("--1vh",`${vph*0.01}px`);
}

/*------- MASK SVGS -------*/
// play icon
// flaticon.com/free-icon/play_727245
let xhkot = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' width='512' height='512' x='0' y='0' viewBox='0 0 320.001 320.001' style='enable-background:new 0 0 512 512' xml:space='preserve' class=''><g><path d='m295.84 146.049-256-144a16.026 16.026 0 0 0-15.904.128A15.986 15.986 0 0 0 16 16.001v288a15.986 15.986 0 0 0 7.936 13.824A16.144 16.144 0 0 0 32 320.001c2.688 0 5.408-.672 7.84-2.048l256-144c5.024-2.848 8.16-8.16 8.16-13.952s-3.136-11.104-8.16-13.952z' fill='black' data-original='black' class=''></path></g></svg>";

// pause icon
// flaticon.com/free-icon/pause_9847130
let vteumj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' width='512' height='512' x='0' y='0' viewBox='0 0 24 24' style='enable-background:new 0 0 512 512' xml:space='preserve' class=''><g><g data-name='Layer 2'><rect width='5' height='20' x='4.5' y='2' rx='2.5' fill='black' data-original='black' class=''></rect><rect width='5' height='20' x='14.5' y='2' rx='2.5' fill='black' data-original='black' class=''></rect></g></g></svg>";

// vroom vroom
const getSpeed = (s) => {
  let res;
  let nums = Number(s.replace(/[^\d\.]*/g,""));
  let units = s.toLowerCase().replace(/[^a-z]/g,"");
  units == "s" ? res = nums*1000 : res = nums;
  return res
}

/*--------- MAIN SHIT ---------*/

// audio volume
let audVol = getRoot("--Audio-Post-Volume");
audVol = audVol.trim() == "" ? 1 : audVol.indexOf("%") > -1 ? parseFloat(audVol) / 100 : 1;

/*------- REMOVE HREF.LI LINK REFERRER -------*/
/*
  shoutout: based on / inspired by @magnusthemes:
  v1: magnusthemes.tumblr.com/post/162657076440
  v2: magnusthemes.tumblr.com/post/643460974823833600
*/
const noHrefLi = () => {
  document.querySelectorAll("a[href*='href.li/?']")?.forEach(a => {
    let href = a.href;
    if(href.indexOf("href.li/?") > -1){
      a.href = href.replace("href.li/?","")
    }
  })

  // EXAMPLE: https://t.umblr.com/redirect?z=https%3A%2F%2Fgithub.com%2Feramdam%2FGifsOnTumblr&t=MDU4MDAzOTZkMmM5NmIzNmNjNzJmM2NlNzRkZjY1NWRlYTQyNGM3NiwwZmQwNjM0ODY0ZDBkY2ZlNWVmMjBhNjgyNTJlMDkzNTU2MTUwZTEy
  // SEEN IN THIS POST: felicitysmoak.tumblr.com/post/188159105501
  document.querySelectorAll("a[href*='t.umblr.com/redirect?z=']")?.forEach(a => {
    let href = a.href;
    if(href.indexOf("t.umblr.com/redirect?z=") > -1){
      let de = decodeURIComponent(href);
      a.href = de.replace("https://t.umblr.com/redirect?z=","").split("&t=")[0]
    }
  })
}

/*------- REMOVE EMPTY ELEMENTS -------*/
const removeEmptyStuff = () => {
  document.querySelectorAll(".post-body")?.forEach(p => {
    if(p.firstElementChild){
      if(p.querySelector("br:only-child")){
        p.querySelector("br:only-child").remove();
        if(p.innerHTML.trim() == ""){
          p.remove()
        }
      }
    }
  })

  setTimeout(() => {
    let stuff = ".comment-body > p:first-child, .comment-body, .comment, .comments, .text-block";

    document.querySelectorAll(stuff)?.forEach(el => {
      if(el.innerHTML.trim() == ""){
        el.remove()
      }
    })

    document.querySelectorAll(".text-block")?.forEach(el => {
      if(el.innerHTML.trim() == ""){
        el.remove()
      }
    })
  },0)
}

/*------- CHANGE .gifv TO .gif -------*/
const removeGIFv = () => {
  document.querySelectorAll("img[src$='.gifv']")?.forEach(img => {
    img.src = img.src.slice(0,-1);
  })
}

/*------- SCROLL TO POSTS BTN -------*/
const scrollThing = () => {
  document.querySelectorAll(".scroll-btn")?.forEach(e => {
    e.addEventListener("click", () => {
      let header = document.querySelector(".header-hero");
      let main = document.querySelector("main");
      if(header && main && typeof animateScrollTo === "function"){
        // github.com/Stanko/animated-scroll-to
        animateScrollTo([null, header.offsetHeight], {
          speed: getSpeed(getRoot("--Header-Scroll-Animation-Speed")),
          easing: (t) => { return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t } // easeInOutQuad
        });
      }
    })
  })
}

/*------- SIDEBAR -------*/
// FloatSidebar.js by vursen
// github.com/vursen/FloatSidebar.js
const sidebarStuff = () => {
  if(typeof FloatSidebar == "function" && currentPath !== "/customize_preview_receiver.html"){
    let topGap = document.querySelector("#efztplipq").offsetHeight; // calc(var(--Top-Bar-Height) + var(--Screen-Gaps))
    let botGap = document.querySelector("#wvqwfcodi").offsetHeight; // var(--Screen-Gaps)
    let sidebar  = document.querySelector(".left-sidebar");
    let sidebarInner = sidebar.querySelector(".sb-inner");
    let relative = document.querySelector(".all-posts");

    const floatSidebar = FloatSidebar({
      sidebar,
      sidebarInner,
      relative,
      topSpacing: topGap,
      bottomSpacing: botGap
    });
    
    // the next 2 lines were in the official documentation/installation page
    // but removing these 2 lines will actually stop the "flickering" if the sidebar is shorter than the main content idfk why. but it works without it so whatever lol
    // floatSidebar.forceUpdate();
    // floatSidebar.destroy();
  }

}

/*------- WRAP STRAY TEXT NODES -------*/
const commentBodyNodes = () => {
  document.querySelectorAll(".comment-body")?.forEach(el => {
    let stack = [el];

    while(stack.length > 0){
      // get latest child from stack
      let currentNode = stack.pop();

      // currentNode does NOT have child nodes + is a text node + isn't empty
      if(currentNode.nodeType === 3 && currentNode.data.trim().length > 0){
        let span = document.createElement("span");
        currentNode.before(span);
        span.appendChild(currentNode);
      } else if(currentNode.childNodes.length > 0){
        // currentNode HAS child nodes, add them to the stack
        for(let i=currentNode.childNodes.length-1; i>=0; i--){
          stack.push(currentNode.childNodes[i]);
        }
      }
    }
  })
}

/*------- LEGACY PHOTO(SET)S -------*/
function legacyIMG(w, h, ld, hd){
  this.width = Number(w);
  this.height = Number(h);
  this.low_res = ld;
  this.high_res = hd;
}

const legacyPhotos = () => {
  let old_photoset = document.querySelectorAll(".legacy-photoset[layout]:not([layout=''], .done)");
  old_photoset?.forEach((sett) => {
    let sett_ly = sett.getAttribute("layout");
    let sett_ar = [];
    let sett_xy = sett_ly.split("");

    sett_xy?.forEach(zett => {
      sett_ar.push(zett)
    })

    sett_ar.forEach((xett, i) => {
      let pylnn = document.createElement("div");
      pylnn.classList.add("layout-row");
      pylnn.setAttribute("cols", xett);
      pylnn.style.gridTemplateColumns = `repeat(${xett}, 1fr)`
      sett.append(pylnn);
    });

    sett.querySelectorAll(".layout-row:empty").forEach((lr, i) => {
      let curNum = parseInt(lr.getAttribute("cols"));
      sett.querySelectorAll("img").forEach((boo, i) => {
        i += 1;
        if(i <= curNum){
          lr.append(boo);
        }

        loadImage(boo.src).then(i => {
          let h = boo.offsetHeight;
          boo.setAttribute("offset-height",h)
        }).catch(err => console.error(err));
      });
    });
    
    sett.classList.add("done");

  }); // end .legacy-photoset forEach
  
  // legacy photoset row heights
  document.querySelectorAll(".layout-row[cols]")?.forEach(row => {
    let a = Date.now();
    let b = 6000;
    let c = setInterval(() => {
      if(Date.now - a > b){
        clearInterval(c)
      } else {
        if(!row.querySelector("img:not([offset-height]:not([offset-height='']))")){
          clearInterval(c);
          let cols = Array.from(row.querySelectorAll(":scope > img[offset-height]:not([offset-height=''])"));
          let minHeight = Math.min(...cols.map(col => Number(col.getAttribute("offset-height"))));
          row.style.height = `${minHeight}px`;
          row.querySelectorAll(":scope > img[offset-height]:not([offset-height=''])")?.forEach(i => {
            i.style.marginTop = `calc(0px - (${i.getAttribute("offset-height")}px - ${minHeight}px) / 2)`
          })
          row.classList.add("done");
          legacyPhotosHeights()
        }
      }
    })
  })
  
  // legacy lightboxes
  document.querySelectorAll(".legacy-photo, .legacy-photoset")?.forEach(sel => {
    let mbusv = [];
    sel.querySelectorAll("img:not(a[onclick*='Tumblr.PanoLightboxInit'] > img)").forEach((imgz, index) => {
      mbusv.push(new legacyIMG(
        imgz.getAttribute("width"),
        imgz.getAttribute("height"),
        imgz.src,
        imgz.src
      ));

      let bipuq = Math.floor(parseInt(index)+1);
      imgz.setAttribute("index",bipuq);
      sel.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(mbusv)},1)`);
      imgz.addEventListener("click", () => {
        sel.setAttribute("onclick",`Tumblr.Lightbox.init(${JSON.stringify(mbusv)},${bipuq})`);
      })
    })
  })
}//end legacyPhotos

// call this on resize only
const legacyPhotosHeights = () => {
  document.querySelectorAll(".photo-block .layout-row[cols]")?.forEach(row => {
    let cols = Array.from(row.querySelectorAll(":scope > img"));
    let minHeight = Math.min(...cols.map(col => col.offsetHeight));
    row.style.height = `${minHeight}px`;
    row.querySelectorAll(":scope > img")?.forEach(i => {
      i.style.marginTop = `calc(0px - (${i.offsetHeight}px - ${minHeight}px) / 2)`
    })
  })
}

/*------- LEGACY VIDEOS -------*/
const legacyVideos = () => {
  document.querySelectorAll(".legacy-video .tumblr_video_container + .poster-thumb[url]:not([url=''])")?.forEach(poster => {
    let posterURL = poster.getAttribute("url");
    if(posterURL.trim() !== ""){
      let segment = posterURL.trim().split(".media.tumblr.com/tumblr_")[1].split("_")[0];

      let vidURL = `https://va.media.tumblr.com/tumblr_${segment}.mp4`
      fetch(vidURL).then(q => {
        if(!q.ok){
          // url does not exist
        }
        return q;
      }).then(c => {
        let newVid = document.createElement("video");
        newVid.src = vidURL;
        newVid.setAttribute("poster",posterURL);
        poster.after(newVid);

        if(typeof VIDYO === "function"){
          VIDYO("video")
        }

        poster.previousElementSibling.remove();
        poster.remove();
      }).catch(err => console.error(err))
    }
  })
}

/*------- QUOTE STUFF -------*/
const quoteStuff = () => {
  // CASE 1: THE QUOTE TEXT IS A <BLOCKQUOTE>
  // sample: glen-test.tumblr.com/post/748319112184430592
  document.querySelectorAll("[post-type='text'] .text-block .comment:first-of-type .comment-body > blockquote:first-child + p:last-child")?.forEach(peas => {
    let bbq = peas.previousElementSibling;
    let bbqFirstChild = bbq.querySelector(":scope > *:first-child");
    if(bbqFirstChild){
      if(bbqFirstChild.matches("div:only-child")){
        // note: bbq is the <blockquote>
        bbq.classList.add("npf_quote");
        bbq.classList.add("quote-text");

        if(peas.innerHTML.trim() !== ""){
          peas.classList.add("quote-source")
        }

        let reblogContent = peas.closest(".comment-body");
        if(reblogContent){
          let quoteSet = document.createElement("div");
          quoteSet.classList.add("quote-set");
          reblogContent.prepend(quoteSet);
          reblogContent.querySelectorAll(":scope > *:not(.quote-set)")?.forEach(v => {
            quoteSet.append(v)
          })
          peas.closest(".comment").classList.add("quote-comment");
        }        
      }
    }
  })//end case 1
  
  // CASE 2: THE QUOTE TEXT IS JUST A <P>
  // sample: glen-test.tumblr.com/post/748319206567706624
  document.querySelectorAll("[post-type='text'] .post-body > .text-block:first-child .comment:first-of-type .comment-body > p:first-child + blockquote.npf_indented:last-child")?.forEach(blockquote => {
    let currentQuoteText = blockquote.previousElementSibling;
    
    let set = document.createElement("div");
    set.classList.add("quote-set");
    
    let newBQ = document.createElement("blockquote");
    newBQ.classList.add("npf_quote");
    newBQ.classList.add("quote-text");
    
    let newSrc = document.createElement("p");
    newSrc.classList.add("quote-source");
    
    currentQuoteText.before(set);
    set.append(newBQ);
    set.append(newSrc);
    
    currentQuoteText.querySelectorAll(":scope > *")?.forEach(x => {
      newBQ.append(x);
    })
    
    currentQuoteText.remove();
    
    blockquote.querySelectorAll(":scope > *")?.forEach(x => {
      newSrc.append(x);
    })
    
    set.closest(".comment")?.classList.add("quote-comment");
    
    blockquote.remove();
  })//end case 2
}//end quoteStuff()

/*------- CHAT STUFF -------*/
const chatStuff = () => {
  document.querySelectorAll(`.npf_chat, [data-npf*='{"subtype":"chat"}']`)?.forEach((chat) => {
    let contents = chat.childNodes;
    if(contents){
      let textNodes = Array.from(contents).filter((node) => {
        return node.nodeType === 3 && node.data.trim().length > 0;
      });

      textNodes.forEach((node) => {
        let wrapper = document.createElement("span");
        node.parentNode.insertBefore(wrapper, node);
        wrapper.appendChild(node);
      });
    }

    /*------- CHAT LABEL -------*/
    // YES chat label
    if(chat.querySelector("b")){
      let checkB = chat.querySelectorAll(":scope > b");
      checkB?.forEach(b => {
        if(!b.matches(".chat-label")){
          b.classList.add("chat-label");
        }
      })
    }

    // NO chat label? force it
    else {
      let makeB = document.createElement("b");
      makeB.classList.add("chat-label");
      chat.prepend(makeB)
    }

    /*------- CHAT CONTENT (sentence/line) -------*/
    chat.querySelectorAll(":scope > b.chat-label")?.forEach((label, i) => {
      // .next() is <span>
      let lnext = label.nextElementSibling;
      if(lnext && lnext.matches("span")){
        lnext.classList.add("chat-content");
      }
      // no .next()
      else {
        let sp = document.createElement("span");
        sp.classList.add("chat-content");
        label.after(sp);
      }          
    })

    let chatLabels = chat.querySelectorAll(".chat-label");
    let chatContents = chat.querySelectorAll(".chat-content");

    // loop through each .chat-label and create new p.npf_chat accordingly
    for(let i=0; i<chatLabels.length; i++){
      let newP = document.createElement("p");
      newP.classList.add("npf_chat")
      newP.classList.add("new_chat")

      let newB = document.createElement("b");
      newB.classList.add("chat-label");
      newB.textContent = chatLabels[i].textContent;

      let newSpan = document.createElement("span");
      newSpan.classList.add("chat-content");
      newSpan.textContent = chatContents[i].textContent;

      newP.appendChild(newB);
      newP.appendChild(newSpan);

      chat.before(newP)
    }

    chat.parentNode.removeChild(chat);
  })//end chat forEach


  document.querySelectorAll(`.npf_chat, [data-npf*='{"subtype":"chat"}']`)?.forEach((chat) => {
    // if .chat-content starts with ":"
    // & .chat-label does NOT end with ":"
    // do smth about it
    chat.querySelectorAll(".chat-label + .chat-content")?.forEach(content => {
      let labelText = content.previousElementSibling.textContent.trim();
      let contentText = content.textContent.trim();
      if(contentText.slice(0,1) == ":"){
        if(labelText.slice(-1) !== ":"){
          content.previousElementSibling.append(":");
          content.textContent = content.textContent.slice(1)
        }
      }
    })

    // change into <li>
    let li = document.createElement("li");
    li.classList.add("chat-line");
    chat.before(li);
    li.append(chat);
    chat.replaceWith(...chat.childNodes);
  });

  // wrap all neighboring .chatline[s] into .chatwrap
  document.querySelectorAll("[post-type='text'] *:not(.chat-wrap) li.chat-line")?.forEach(line => {
    let prevEl = line.previousElementSibling;
    if(!prevEl?.matches(".chat-line")){
      let cont = document.createElement("ul");
      cont.classList.add("chat-wrap");
      line.before(cont);

      let apres = cont.nextElementSibling;
      while(apres && apres.matches(".chat-line")){
        cont.appendChild(apres);
        apres = cont.nextElementSibling;
      }
    }
  })

  setTimeout(() => {
    // if chat is the first .comment, hide the comment header
    document.querySelectorAll("[post-type='chat'] .comment:first-child ul.chat-wrap")?.forEach(c => {
      c.closest(".comment")?.classList.add("chat-first-comment")
    })
  },0)

}

/*------- NPF LINK STUFF -------*/
const npfLinkStuff = () => {
  document.querySelectorAll("[post-type='text'] .post-body .text-block .comments .comment-body .npf-link-block")?.forEach(linkBlock => {
    let titleText = "";
    let descText = "";
    let imgURL = "";
    let linkTarget = "";
    let linkHref = "";
    let siteName = "";

    let titleDiv = linkBlock.querySelector(".title");
    titleDiv ? titleText = titleDiv.textContent : null;

    let descDiv = linkBlock.querySelector(".description");
    descDiv ? descText = descDiv.textContent : null;

    let siteDiv = linkBlock.querySelector(".site-name");
    siteDiv ? siteName = siteDiv.textContent : null;

    if(linkBlock.matches(".has-poster")){
      let imgDiv = linkBlock.querySelector(".poster[style*='background-image:url(']");
      imgURL = imgDiv.style.backgroundImage.replace(/(^url\()|(\)$|[\"\'])/g,"");
    }

    let get_a = linkBlock.querySelector(":scope > a");
    if(get_a.matches("[target]")){
      linkTarget = get_a.getAttribute("target");
    }
    if(get_a.matches("[href]")){
      linkHref = get_a.getAttribute("href");
    }
    
    /*===========================================*/
    
    // <a href="{URL}" class="link-render post-title" {Target}>
    let makeA = document.createElement("a");
    makeA.classList.add("link-render");
    makeA.target = linkTarget;
    makeA.href = linkHref;
    
    linkBlock.before(makeA);
    
    // <span class="title-text">
    let textSpan = document.createElement("span");
    
    makeA.append(textSpan);
    
    let subSpan = document.createElement("span");
    subSpan.textContent = titleText.trim();
    
    textSpan.append(subSpan)
    
    // <span class="site-name">
    let siteSpan = document.createElement("span");
    siteSpan.classList.add("site-name");
    
    textSpan.append(siteSpan);
    
    // <i class="ph-bold ph-link"></i>
    let i = document.createElement("i");
    i.classList.add("ph-bold");
    i.classList.add("ph-link");
    
    siteSpan.append(i);
    
    // <span class="host-name">{Host}</span>
    let hostSpan = document.createElement("span");
    hostSpan.classList.add("host-name");
    hostSpan.textContent = siteName.trim();
    
    siteSpan.append(hostSpan);
    
    // remove the original link thing
    linkBlock.remove();
    
    // current comment
    let thisComment = makeA.closest(".comment");
    
    // check if there's stuff (caption text) after the link itself
    let hasNext = makeA.nextElementSibling;
    
    // has .next()
    if(hasNext && hasNext.innerHTML.trim() !== ""){
      // HAS ANOTHER LINK AFTER IT, DON'T RELOCATE
      // DON'T CARE ABOUT ANY CAPTION TEXT YET
      if(hasNext.matches(".npf-link-block") || hasNext.matches(".link-render")){
        let block = makeA.closest(".text-block");
        let comments = makeA.closest(".comments");
        if(block && comments){
          let h2 = document.createElement("h2");
          h2.textContent = subSpan.textContent;
          subSpan.before(h2);
          subSpan.remove();
        }
      }
      
      // IS THE LAST NPF LINK, AND HAS MORE CAPTION TEXT
      // RELOCATE
      else {
        let block = makeA.closest(".text-block");
        let comments = makeA.closest(".comments");
        if(block && comments){
          
          // only relocate if the first thing in the post body ISN'T the post title
          if(block.firstElementChild && !block.firstElementChild.matches(".post-title")){
            makeA.classList.add("post-title");
            textSpan.classList.add("title-text");

            comments.before(makeA);
            block.classList.add("link-block");
            block.classList.remove("text-block");
          }
        }
      }
    }
    
    // no .next();
    // NO CAPTION
    // RELOCATE + REMOVE CURRENT .comment
    else {
      let block = makeA.closest(".text-block");
      let thisComment = makeA.closest(".comment");
      let comments = makeA.closest(".comments");
      if(block && comments){
        // only relocate if the first thing in the post body ISN'T the post title
        if(block.firstElementChild && !block.firstElementChild.matches(".post-title")){
          makeA.classList.add("post-title");
          textSpan.classList.add("title-text");

          comments.before(makeA);
          block.classList.add("link-block");
          block.classList.remove("text-block");
          
          thisComment ? thisComment.remove() : null;
        }
        
        // if there's already a post-title prepended,
        // h2 the link name and leave it there
        else {
          let h2 = document.createElement("h2");
          h2.textContent = subSpan.textContent;
          subSpan.before(h2);
          subSpan.remove();
        }
      }
    }
    
    /*===========================================*/
    
  })//end npf link forEach

  document.querySelectorAll(".link-block a.link-render > h2:first-of-type")?.forEach(h2 => {
    h2.innerHTML = h2.innerHTML.trim();
  })
}

/*------- NPF VIDEO STUFF -------*/
const npfVideoStuff = () => {
  document.querySelectorAll(".tmblr-full video[autoplay]")?.forEach(v => {
    v.autoplay = false;
    v.muted = false;
  })
  
  typeof VIDYO === "function" ? VIDYO("video") : null;
  
  document.querySelectorAll(".npf_row:only-child > .tmblr-full.tmblr-embed:only-child > iframe:only-child")?.forEach(npf_iframe => {
    setTimeout(() => {
      if(npf_iframe.closest(".npf_inst")){
        npf_iframe.closest(".npf_inst").classList.add("npf_video")
      }
    },0)
  })
}

/*------- ASK POST STUFF -------*/
const askStuff = () => {
  document.querySelectorAll(".answer-block .q-top")?.forEach(q => {
    if(q.textContent.trim() !== "" && q.textContent.trim().indexOf(" ") > -1){
      let qtext = q.textContent.trim();
      let askerName = qtext.split(" ")[0];
      let img = q.closest(".answer-block").querySelector(".askerpic");
      img ? img.alt = `${askerName}'s avatar'` : null
    }
  })
  
  document.querySelectorAll("img[src*='assets.tumblr.com/images/anonymous_avatar_']")?.forEach(anonIMG => {
    anonIMG.src = "https://static.tumblr.com/gtjt4bo/7Fascbp81/tumblr_anon_1.png"
  })
}

/*------- READ MORE STUFF -------*/
const readMoreStuff = () => {
  document.querySelectorAll(".comment-body p > a")?.forEach(a => {
    if(a.matches("a.read_more") || a.textContent.trim() == "Keep reading"){
      a.closest("p").classList.add("keep-reading")
    }
  })

  document.querySelectorAll(".posts .comments .comment.original-comment p.keep-reading")?.forEach(r => {
    let rootURL = r.closest(".posts[root-url][username]");
    if(rootURL){
      let username = rootURL.getAttribute("username").trim();
      rootURL = rootURL.getAttribute("root-url").trim();

      let readMoreLink = r.querySelector(":scope > a[href]");
      if(readMoreLink){
        if(readMoreLink.getAttribute("href") !== rootURL){
          readMoreLink.href = rootURL;

          let head = r.closest(".original-comment").querySelector(".comment-header[href]");
          if(head){
            if(head.getAttribute("href") !== rootURL){
              head.href = rootURL;

              head.querySelector(".username").textContent = username;                

              fetch(`https://api.tumblr.com/v2/blog/${username}/avatar`, {
                method: "GET",
                mode: 'no-cors'
              })
              .then(r => {
                let contentType = r.headers.get("content-type");
                if(contentType && contentType.indexOf("application/json") !== -1){
                  return r.json().then(data => {
                    // is "not found"
                  });
                } else {
                  return r.text().then(text => {
                    head.querySelector("img.userpic").src = `https://api.tumblr.com/v2/blog/${username}/avatar`
                  });
                }
              }).catch(err => console.error(err));
            }
          }
        }
      }
    }
  })
}

/*------- LEGACY AUDIO -------*/
const legacyAudio = () => {
  document.querySelectorAll(".aud-gen .aud-info + .aud-iframe iframe.tumblr_audio_player")?.forEach((aud_embed) => {
    let audPostID = aud_embed.closest("[post-type='audio'][id]").getAttribute("id").replaceAll("post-","");

    let audioIMGArea = aud_embed.closest(".aud-gen")?.querySelector(".aud-cover img");
    let audioPlay = aud_embed.closest(".aud-gen")?.querySelector(".q-play");
    let audioPause = aud_embed.closest(".aud-gen")?.querySelector(".q-pause");
    let audioTextArea = aud_embed.closest(".aud-iframe")?.previousElementSibling;
    let audioDLArea = aud_embed.closest(".aud-gen")?.querySelector(".aud-dl");

    aud_embed.addEventListener("load", () => {
      let contents = aud_embed.contentDocument;

      let audSrc, audTitle, audArtist, audAlbumIMG, audAlbumName;

      // IS NULL
      if(contents === null){
        let useSrc = aud_embed.src;
        let trem = useSrc.split("?audio_file=")[1].split("&")[0];
        audSrc = decodeURIComponent(trem)
      }

      // IS NOT NULL
      else {
        audSrc = contents.querySelector("[data-stream-url]")?.getAttribute("data-stream-url");
        audTitle = contents.querySelector("[data-track]")?.getAttribute("data-track");
        audArtist = contents.querySelector("[data-artist]")?.getAttribute("data-artist");
        audAlbumIMG = contents.querySelector("[data-album-art]")?.getAttribute("data-album-art");
        audAlbumName = contents.querySelector("[data-album]")?.getAttribute("data-album");
      }//end: is NOT null

      // bind audio url to download button
      audioDLArea.href = audSrc;

      // create an audio element
      let newAud = document.createElement("audio");
      newAud.src = audSrc;
      aud_embed.closest(".aud-gen").append(newAud);
      newAud.volume = audVol;

      audioPlay.addEventListener("click", () => {
        if(newAud.paused){
          newAud.play();
        }
      });

      audioPause.addEventListener("click", () => {
        if(!newAud.paused){
          newAud.pause();
        }
      });

      newAud.addEventListener("play", () => {
        audioPlay.style.display = "none";
        audioPause.style.display = "flex";
      });

      newAud.addEventListener("pause", () => {
        audioPause.style.display = "none";
        audioPlay.style.display = "flex";
      });

      newAud.addEventListener("ended", () => {
        audioPause.style.display = "none";
        audioPlay.style.display = "flex";
      });
    }); //end audio load
  }); // end audio each (legacy)
}

/*------- NPF AUDIO -------*/

const neueAudio = () => {
  let hideEmptyNPFAudioInfo = getRoot("--Hide-Empty-NPF-Audio-Info");
  document.querySelectorAll("figcaption.audio-caption")?.forEach((npfAudio) => {
    // check if there's anything preceding text/content
    let prev = npfAudio.previousElementSibling;

    // check if there's anything after it
    let next = npfAudio.nextElementSibling;

    if(next){
      if(next.innerHTML.trim() == ""){
        npfAudio.classList.add("no-next");
      }
    } else {
      npfAudio.classList.add("no-next");
    }

    if(prev){
      if(prev.innerHTML.trim() == ""){
        npfAudio.classList.add("no-prev");
      }
    } else {
      npfAudio.classList.add("no-prev");
    }

    npfAudio.classList.remove("no-prev");
    npfAudio.classList.remove("no-next");

    let npfAudioDiv = document.createElement("div");
    npfAudioDiv.classList.add("aud-gen");
    npfAudio.before(npfAudioDiv);

    npfAudio.querySelectorAll(":scope > *").forEach((h) => {
      npfAudioDiv.append(h);
    });

    npfAudio.remove();

    npfAudioDiv.querySelectorAll(".tmblr-audio-meta").forEach((m) => {
      m.classList.remove("tmblr-audio-meta");
    });

    let deets = document.createElement("div");
    deets.classList.add("aud-info");

    let oldDeets = npfAudioDiv.querySelector(".audio-details");
    oldDeets.before(deets);
    oldDeets.querySelectorAll(":scope > *").forEach((o) => {
      deets.append(o);
    });

    oldDeets.remove();

    let xyz = document.createElement("div");
    xyz.classList.add("aud-xyz");
    deets.before(xyz);
    xyz.append(deets);

    let npfAudioTitle = npfAudioDiv.querySelector(".title");
    let npfAudioArtist = npfAudioDiv.querySelector(".artist");
    let npfAudioAlbum = npfAudioDiv.querySelector(".album");

    if(npfAudioTitle){
      let titleText = npfAudioTitle.textContent;
      if(titleText.trim() == "" && hideEmptyNPFAudioInfo == "no"){
        titleText = "Untitled Track";
      }
      let titleDiv = document.createElement("div");
      titleDiv.classList.add("aud-title");
      titleDiv.textContent = titleText;
      npfAudioTitle.before(titleDiv);
      npfAudioTitle.remove();
    }

    if(npfAudioArtist){
      let artistText = npfAudioArtist.textContent;
      if(artistText.trim() == "" && hideEmptyNPFAudioInfo == "no"){
        artistText = "Unknown Artist";
      }
      let artistDiv = document.createElement("div");
      artistDiv.classList.add("aud-artist");
      artistDiv.textContent = artistText;
      npfAudioArtist.before(artistDiv);
      npfAudioArtist.remove();
    }

    if(npfAudioAlbum){
      let albumText = npfAudioAlbum.textContent;
      if(albumText.trim() == "" && hideEmptyNPFAudioInfo == "no"){
        albumText = "Unknown Album";
      }
      let albumDiv = document.createElement("div");
      albumDiv.classList.add("aud-album");
      albumDiv.textContent = albumText;
      npfAudioAlbum.before(albumDiv);
      npfAudioAlbum.remove();
    }

    let aftermath = xyz.nextElementSibling;
    if(aftermath){
      if(aftermath.matches("img.album-cover")){
        xyz.before(aftermath); // put <img class='album-cover'> BEFORE .aud-xyz
        aftermath.classList.remove("album-cover");
      } else {
        // if .aud-xyz.next() !== img.album-cover,
        // make it
        let makeTheCover = document.createElement("img");
        makeTheCover.src = "https://assets.tumblr.com/images/x.gif";
        xyz.before(makeTheCover);
      }
    }

    // if there is no .aud-xyz.next(),
    // make img.album-cover
    else {
      let makeTheCover = document.createElement("img");
      makeTheCover.src = "https://assets.tumblr.com/images/x.gif";
      xyz.before(makeTheCover);
    }

    let audCover = document.createElement("div");
    audCover.classList.add("aud-cover");

    if(aftermath?.matches("img.album-cover")){
      aftermath.before(audCover);
      audCover.append(aftermath);
    } else {
      xyz.before(audCover);
      audCover.append(audCover.closest(".aud-gen").querySelector("img"));
    }

    let audCtl = document.createElement("div");
    audCtl.classList.add("aud-ctl");
    audCover.prepend(audCtl);

    // play container
    let cplay = document.createElement("button");
    cplay.classList.add("q-play");
    cplay.ariaLabel = "Play";
    audCtl.append(cplay);

    // play icon
    let playIcon = document.createElement("span");
    playIcon.classList.add("play-icon");
    playIcon.ariaHidden = true;
    cplay.append(playIcon);

    // pause container
    let cpause = document.createElement("button");
    cpause.classList.add("q-pause");
    cpause.ariaLabel = "Pause";
    audCtl.append(cpause);

    // pause icon
    let pauseIcon = document.createElement("span");
    pauseIcon.classList.add("pause-icon");
    pauseIcon.ariaHidden = true;
    cpause.append(pauseIcon);

    // audio url
    let audSrc;
    let actualAud = npfAudioDiv.nextElementSibling; // was xyz.nextElementSibling
    if(actualAud){
      if(actualAud.matches("audio")){
        if(actualAud.matches("[src]")){
          audSrc = actualAud.getAttribute("src");
        } else if(actualAud.querySelector("source[src]")){
          audSrc = actualAud.querySelector("source[src]").getAttribute("src");
        }
      }
    }

    // make audio btn <a>
    let a = document.createElement("a");
    a.classList.add("aud-dl");
    a.href = audSrc;
    a.target = "_blank";
    a.ariaLabel = "Download";
    xyz.after(a);

    // make audio dl icon
    let ic = document.createElement("i");
    ic.classList.add("mgc_download_2_line");
    a.append(ic);

    // set the volume
    actualAud.volume = audVol;

    // play and pause events
    cplay.addEventListener("click", () => {
      if(actualAud.paused){
        actualAud.play();
      }
    });

    cpause.addEventListener("click", () => {
      if(!actualAud.paused){
        actualAud.pause();
      }
    });

    actualAud.addEventListener("play", () => {
      cplay.style.display = "none";
      cpause.style.display = "flex";
    });

    actualAud.addEventListener("pause", () => {
      cpause.style.display = "none";
      cplay.style.display = "flex";
    });

    actualAud.addEventListener("ended", () => {
      cpause.style.display = "none";
      cplay.style.display = "flex";

      // autoplay the next audio if there is one
      // npfAudioDiv == .aud-gen
      let figNext = npfAudioDiv.closest(".npf_audio").nextElementSibling; /* was npfAudioDiv.nextElementSibling */
      if(figNext && figNext.matches(".npf_audio")){
        figNext.querySelector(".q-play")?.click();
      }
    });
  }); //end npfAudio each

  setTimeout(() => {
    document.querySelectorAll(".npf_group .npf_inst.npf_audio:first-child")?.forEach(i => {
      let parent = i.parentNode;
      if(!parent.querySelector(":scope > .npf_inst:not(.npf_audio)")){
        i.closest(".npf_group").classList.add("npf-audio-only")
      }
    })
  },0)
}; //end neueAudio()

/*------- SOUNDCLOUD -------*/
const soundcloudStuff = () => {
  let player_btn_color = getRoot("--Audio-Post-Btns-BG");
  let soundcloud_height = Number(getRoot("--SoundCloud-Player-Height").replace(/[^\d\.]*/g,""));
  let scAlbumShowHide = getRoot("--SoundCloud-Show-Album-Image");
  soundcloud_height = !soundcloud_height ? 116 : soundcloud_height;
  scAlbumShowHide = scAlbumShowHide == "yes" ? "true" : "false";
  document.body?.classList.add(`sc-${soundcloud_height}`);
  
  // minimalist soundcloud player: @shythemes
  // shythemes.tumblr.com/post/114792480648
  document.querySelectorAll("iframe[src*='soundcloud.com']")?.forEach((sc) => {
    let curSrc = sc.getAttribute("src").split("&")[0];
    sc.src = `${curSrc}&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=${scAlbumShowHide}&amp;origin=tumblr&amp;color=${player_btn_color.split("#")[1]}`;
    (sc.height = soundcloud_height), (sc.width = "100%");

    setTimeout(() => {
      soundcloud_height == 20 ? sc.closest("figure")?.classList.add("sc-short") : null
    },0)
  });
}

/*------- STOP AUDIOS FROM PLAYING OVER E/O -------*/
// credit: stackoverflow.com/a/19792168/8144506
const audioNoConflict = () => {
  document.addEventListener("play", (e) => {
    let auds = document.querySelectorAll("audio");
    auds?.forEach((a,j) => {
      if(auds[j] != e.target){
        auds[j].pause();
        auds[j].currentTime = 0;
      }
    })
  },true)
}

/*------- REASSIGN POST TYPES -------*/
const redoPostTypes = () => {
  setTimeout(() => {
    document.querySelectorAll("[post-type='text'] .post-body > *:first-child")?.forEach(target => {
      let post = target.closest("[post-type='text']");

      if(target.matches(".photo-origin")){
        // link
        if(target.matches(".link-render")){
          post.classList.add("ex-npf");
          post.setAttribute("post-type","link")
        }

        let innerFirst = target.firstElementChild;
        if(!innerFirst) return;

        // image
        if(innerFirst.matches(".npf_inst.npf_photo")){
          post.classList.add("ex-npf");
          post.setAttribute("post-type","photo")
        }

        // audio
        else if(innerFirst.matches(".npf_inst.npf_audio")){
          post.classList.add("ex-npf");
          post.setAttribute("post-type","audio")
        }

        // video
        else if(innerFirst.matches(".npf_inst.npf_video")){
          post.classList.add("ex-npf");
          post.setAttribute("post-type","video")
        }
      }//end: if: is .photo-origin
    })//end: post each

    document.querySelectorAll("[post-type='text'] .post-body .comments .comment:first-child .comment-body > *:first-child")?.forEach(firstCommentBody => {
      let post = firstCommentBody.closest("[post-type='text']");

      // poll
      if(firstCommentBody.matches(".poll-post")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","poll")
      }

      // chat
      else if(firstCommentBody.matches(".chat-wrap")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","chat")
      }

      // quote
      else if(firstCommentBody.matches(".quote-set")){
        post.classList.add("ex-npf");
        post.setAttribute("post-type","quote")
      }
    })//end: post each
  },0)
}

/*------- CHANGE CURLY QUOTES TO STRAIGHT ONES -------*/
const uncurly = () => {
  document.querySelectorAll("pre, code, .npf_chat, .chat-content")?.forEach((code) => {
    let stuff = code.innerHTML;
    stuff = stuff.replace(/\u201C/g,'"').replace(/\u201D/g,'"')
    code.innerHTML = stuff;
  });
}

/*------- DEACTIVATED USERS -------*/
const deactUsers = () => {
  /*---- reblog heads ---*/
  document.querySelectorAll(".comment.deactivated[username]:not([username=''])")?.forEach(user => {
    let username = user.getAttribute("username");
    let usernameText = username.trim();
    
    if(usernameText.indexOf("-deac") > -1){
      let avantDash = username.substring(0,username.lastIndexOf("-"));

      let usernameDiv = user.querySelector(".username");
      let deacDiv = user.querySelector(".deactivated");

      usernameDiv ? usernameDiv.textContent = avantDash : null;
    }
  })//end reblog heads (comments)
  
  /*---- answerer ---*/
  document.querySelectorAll(".answer-block .comment.original-comment[username]:not([username=''])")?.forEach(user => {
    let answeredLabel = "";

    let username = user.getAttribute("username");
    let usernameText = username.trim();
    
    if(usernameText.indexOf("-deac") > -1){
      let avantDash = username.substring(0,username.lastIndexOf("-"));

      let usernameDiv = user.querySelector(".username");
      let deacDiv = user.querySelector(".deactivated");

      if(usernameDiv && !deacDiv){
        answeredLabel = " " + usernameDiv.textContent.split(" ")[1];
        usernameDiv.textContent = avantDash

        let d = document.createElement("span");
        d.classList.add("deactivated");
        d.textContent = "(deactivated)";
        usernameDiv.after(d);
        
        usernameDiv.append(answeredLabel)
      }
    }
  })//end answerer
  
  /*---- "person" icon OP hover ---*/
  document.querySelectorAll(".op-area a[aria-label]:not([aria-label=''])")?.forEach(source => {
    let label = source.getAttribute("aria-label");
    let by = label.split(" ")[0];
    let rest = label.split(" ")[1];
    
    if(rest.indexOf("-deac") > -1){
      let avantDash = rest.substring(0,rest.lastIndexOf("-"));

      source.ariaLabel = `${avantDash} (deactivated)`
    }
  })
  
  /*---- via & source ---*/
  document.querySelectorAll(".via-part a, .src-part a")?.forEach(user => {
    let username = user.textContent;
    let usernameText = username.trim();
    
    if(usernameText.indexOf("-deac") > -1){
      let avantDash = username.substring(0,username.lastIndexOf("-"));

      user.textContent = `${avantDash} (deactivated)`
    }
  })//end via & src
}

/*------- PREPEND NPF POST TITLES -------*/
const npfPostTitles = () => {
  document.querySelectorAll(".posts .text-block .comments > .comment:first-child .comment-body > *:first-child")?.forEach(heading => {
    if(heading.matches("h1") || heading.matches("h2")){
      let next = heading.nextElementSibling;
      if(next && next.innerHTML.trim() !== ""){
        // counter cases of: title -> npf_group -> more stuff [?]
        // if npf_group follows title, the npf_group gets prepended fsfr
        // post: tumblr.com/glen-test/737099763156123648
        if(next.matches(".npf_group")){
          let pain = document.createElement("div");
          pain.setAttribute("hidden","");
          heading.after(pain);
          
          heading.classList.add("post-title");
          heading.closest(".text-block").prepend(heading);
          
          let span = heading.querySelector("span");
          if(span){
            span.classList.add("title-text");
          }
        }
        
        else {          
          heading.classList.add("post-title");
          heading.closest(".text-block").prepend(heading);
          
          let span = heading.querySelector("span");
          if(span){
            span.classList.add("title-text");
          }
        }
      }
    }
  })
}

/*------- TOGGLE TAGS -------*/
const togTags = () => {
  document.querySelectorAll(".posts .tags-press")?.forEach(tagsTrigger => {
    tagsTrigger.addEventListener("click", () => {
      let tagscont = tagsTrigger.closest(".posts").querySelector(".tagscont");
      if(tagscont){
        !tagscont.matches(".expand") ? tagscont.classList.add("expand") : tagscont.classList.remove("expand");
      }      
    })
  })
}

/*------- OVERSCROLL Y/N -------*/
const overscroll = () => {
  // prevent trackpad overscroll ONLY on desktops
  if(/Mobi|Android/i.test(navigator.userAgent)){
    // on mobile
    document.querySelectorAll("html, body")?.forEach(s => {
      s.matches(".no-overscroll") ? s.classList.remove("no-overscroll") : null        
    })
  }

  else {
    // on desktop
    document.querySelectorAll("html, body")?.forEach(s => {
      !s.matches(".no-overscroll") ? s.classList.add("no-overscroll") : null      
    })
  }
}

/*------- TURN [title] TO [aria-label] -------*/
const ariaLabels = () => {
  document.querySelectorAll("[title]:not([title=''])")?.forEach(t => {
    let tt = t.title;
    t.removeAttribute("title");
    t.setAttribute("aria-label",tt);
  })
}

/*------- TOOLTIPS -------*/
const tippys = (el) => {
  document.querySelectorAll(`[${el}]:not([${el}=''])`)?.forEach(x => {
    if(x.getAttribute(el).trim() !== ""){
      if(el === "title"){
        let ogTitle = x.getAttribute(el);
        x.addEventListener("mouseenter", () => {
          x.removeAttribute("title")
        })

        x.addEventListener("mouseleave", () => {
          x.setAttribute("title",ogTitle)
        })
      }
      tippy(x, {
        content: x.getAttribute(el),
        maxWidth: "var(--Post-Width)",
        followCursor: true,
        arrow: false,
        offset: [0, 25],
        moveTransition: "transform 0.015s ease-out",
      })
    }
  })
}

/*------- FADE IN STUFF AFTER INITIALIZING -------*/
const fadeLoad = () => {
  let vp = getSpeed(getRoot("--Load-In-Delay"));
  let sp = getSpeed(getRoot("--Load-In-Speed"));
  vp = vp === "" ? 0 : vp;
  sp = sp === "" ? 400 : sp;

  document.querySelectorAll(".load-in:not(.load-removing)")?.forEach(el => {
    setTimeout(() => {
      el.classList.remove("load-in");
      el.classList.add("load-removing");
      setTimeout(() => {
        el.classList.remove("load-removing");
        el.getAttribute("class").trim() == "" ? el.removeAttribute("class") : null
      },sp)
    },vp)
  })
}

/*----- CUSTOMIZE PAGE MSG -----*/
const custard = () => {
  if(currentPath == "/customize_preview_receiver.html"){
    let uivhw = document.createElement("div");
    uivhw.id = "uivhw";
    
    let p = document.createElement("p");
    p.innerHTML = `Thank you for using this theme!<br>Please read <a href="https://dub.sh/lachesis-guide" target="_blank" aria-label="Opens in a new tab">the guide</a> to help with editing.`;
    
    document.body.append(uivhw);
    uivhw.append(p);
  }
}

/*------- THEME INIT -------*/
const themeInit = () => {
  document.querySelectorAll(".tumblr_preview_marker___")?.forEach(m => {
      m.remove();
  })
  
  // housekeeping stuff:
  noHrefLi();
  commentBodyNodes();
  
  // essential theme stuff:
  npfPostTitles();
  legacyPhotos();
  legacyVideos();
  quoteStuff();
  chatStuff();
  npfLinkStuff();
  npfVideoStuff();
  askStuff();
  readMoreStuff();
  
  // audio stuff
  legacyAudio();
  neueAudio();
  soundcloudStuff();
  audioNoConflict();
  
  // extra theme stuff:
  removeGIFv();
  scrollThing();
  sidebarStuff();
  
  redoPostTypes();
  removeEmptyStuff();
  uncurly();
  deactUsers();
  
  // lachesis-specific stuff
  document.querySelectorAll(".posts .post-controls")?.forEach(c => {
    let phoBot = c.closest(".posts").querySelector(".pho-bot");
    if(phoBot){
      phoBot.style.setProperty("--Post-Controls-Count",c.querySelectorAll(":scope > [class*='-area']").length)
    }    
  })
  togTags();
  custard();
  
  ariaLabels();
  tippys("aria-label");
  
  fadeLoad();  
  
  // smooth scroll(ing)
  if(!document.documentElement.matches("[smooth-scroll='yes']")){
    SmoothScroll.destroy();
  }

}//end themeInit()

/*------- ON INITIAL LOAD -------*/
document.addEventListener("DOMContentLoaded", () => {
  screenDims();
  document.querySelector("body")?.style.setProperty("--Audio-Post-Play-Btn-SVG", `url("${xhkot}")`);
  document.querySelector("body")?.style.setProperty("--Audio-Post-Pause-Btn-SVG",`url("${vteumj}")`);
  themeInit();
  overscroll();
})//end DOMContentLoaded

window.addEventListener("resize", () => {
  legacyPhotosHeights();
  screenDims();
})

/* note: the following CANNOT be 'const' */
function tumblrNotesInserted(){
  ariaLabels();
  tippys("aria-label");
  
  // making sure that "JohnSmith posted this" is always the last item otherwise it's weird lol
  document.querySelectorAll("ol.notes li.original_post")?.forEach(john => {
    if(john.nextElementSibling && john.nextElementSibling.matches("li:not(.more_notes_link_container)")){
      john.remove();
    }
  })
}